/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizal.store.service;

import com.rizal.store.model.SalesDetail;
import com.rizal.store.repositories.SalesDetailRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author GodSpeed
 */
@Service
public class SalesDetailServiceImpl implements SalesDetailService{
    
    private final SalesDetailRepository salesDetailRepository;

    @Autowired
    public SalesDetailServiceImpl(SalesDetailRepository salesDetailRepository) {
        this.salesDetailRepository = salesDetailRepository;
    }

    @Override
    public List<SalesDetail> getAll() {
        return salesDetailRepository.findAll();
    }

    @Override
    public Optional<SalesDetail> getSalesDetailById(Long id) {
        return salesDetailRepository.findById(id);
    }

    @Override
    public void save(SalesDetail salesDetail) {
        salesDetailRepository.save(salesDetail);
    }
    
    @Override
    public void save(List<SalesDetail> salesDetails) {
        SalesDetail newRow = new SalesDetail();
        for (SalesDetail salesDetail : salesDetails) {
            save(salesDetail);
        }
    }

    @Override
    public void delete(Long id) {
        salesDetailRepository.deleteById(id);
    }

    @Override
    public List<SalesDetail> selectBySalesId(Long id) {
        return salesDetailRepository.selectBySalesId(id);
    }

}
