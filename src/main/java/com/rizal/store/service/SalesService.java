/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizal.store.service;

import com.rizal.store.model.Sales;
import com.rizal.store.model.form.SalesForm;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author GodSpeed
 */
public interface SalesService {
    
    List<Sales> getAll();
    
    Optional<Sales> getSalesById(Long id);
    
    List<SalesForm> findAsForm();
    
    void save(Sales sales);
    
    void save(SalesForm salesForm);
    
    void delete(Long id);
}
