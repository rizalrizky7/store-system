package com.rizal.store.service;

import com.rizal.store.model.User;
import com.rizal.store.web.dto.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User save(UserRegistrationDto registrationDto);
}
