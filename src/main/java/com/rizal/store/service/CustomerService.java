/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizal.store.service;

import com.rizal.store.model.Customer;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

/**
 *
 * @author GodSpeed
 */
public interface CustomerService {
    
    List<Customer> getAllCustomers();
    
    Optional<Customer> getCustomerById(Long id);
    
    void save(Customer customer);
    
    void delete(Long id);
}
