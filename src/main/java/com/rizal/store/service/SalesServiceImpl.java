/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizal.store.service;

import com.rizal.store.model.Sales;
import com.rizal.store.model.SalesDetail;
import com.rizal.store.model.form.SalesForm;
import com.rizal.store.repositories.SalesRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author GodSpeed
 */
@Service
public class SalesServiceImpl implements SalesService {

    private final SalesRepository salesRepository;
    private final SalesDetailService salesDetailService;

    @Autowired
    public SalesServiceImpl(SalesRepository salesRepository, SalesDetailService salesDetailService) {
        this.salesRepository = salesRepository;
        this.salesDetailService = salesDetailService;
    }

    @Override
    public List<Sales> getAll() {
        return salesRepository.findAll();
    }

    @Override
    public List<SalesForm> findAsForm() {
        List<SalesForm> forms = new ArrayList<>();
        List<Sales> sales = salesRepository.findAll();
        for (Sales sale : sales) {
            SalesForm form = new SalesForm();
            List<SalesDetail> details = salesDetailService.selectBySalesId(sale.getId());
            form.setSales(sale);
            form.setDetails(details);
            forms.add(form);
        }
        return forms;
    }

    @Override
    public Optional<Sales> getSalesById(Long id) {
        return salesRepository.findById(id);
    }

    private String getLast(Date transactionDate) {
        String last = salesRepository.getLastId(transactionDate);
        Long lastId = Long.valueOf(last != null ? last : "0");
        Long nextId = lastId + 1;
        String code = String.format("%04d", nextId);
        return code;
    }

    @Override
    public void save(Sales sales) {
        if(sales.getCode() == null || sales.getCode().isEmpty()){
            String code = getLast(sales.getTrxDate());
            sales.setCode(sales.getTrxDateFormatted() + "-" + code);
        }
        salesRepository.save(sales);
    }

    @Override
    public void save(SalesForm salesForm) {
        Sales newRow = salesForm.getSales();
        save(newRow);
        for (SalesDetail detail : salesForm.getDetails()) {
            detail.setSales(newRow);
            salesDetailService.save(detail);
        };
    }

    @Override
    public void delete(Long id) {
        salesRepository.deleteById(id);
    }

}
