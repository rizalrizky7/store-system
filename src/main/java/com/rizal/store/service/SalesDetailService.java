/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizal.store.service;

import com.rizal.store.model.SalesDetail;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author GodSpeed
 */
public interface SalesDetailService {
    
    List<SalesDetail> getAll();
    
    List<SalesDetail> selectBySalesId(Long id);
    
    Optional<SalesDetail> getSalesDetailById(Long id);
    
    void save(SalesDetail salesDetail);
    
    void save(List<SalesDetail> salesDetails);
    
    void delete(Long id);
}
