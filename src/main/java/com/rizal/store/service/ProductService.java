/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizal.store.service;

import com.rizal.store.model.Product;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author GodSpeed
 */
public interface ProductService {
    
    List<Product> getAllProducts();
    
    Optional<Product> getProductById(Long id);
    
    void saveProduct(Product product);
    
    void deleteProduct(Long id);
}

