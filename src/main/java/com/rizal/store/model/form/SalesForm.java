/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizal.store.model.form;

import com.rizal.store.model.Sales;
import com.rizal.store.model.SalesDetail;
import java.util.List;

/**
 *
 * @author GodSpeed
 */
public class SalesForm {
    
    private Sales sales;
    private List<SalesDetail> details;

    public SalesForm() {
    }

    public SalesForm(Sales sales, List<SalesDetail> details) {
        this.sales = sales;
        this.details = details;
    }

    public Sales getSales() {
        return sales;
    }

    public void setSales(Sales sales) {
        this.sales = sales;
    }

    public List<SalesDetail> getDetails() {
        return details;
    }

    public void setDetails(List<SalesDetail> details) {
        this.details = details;
    }
    
    
}
