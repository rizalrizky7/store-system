/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizal.store.model;

import com.fasterxml.jackson.annotation.JsonView;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author GodSpeed
 */
@Entity
@Table(name = "sales")
public class Sales {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date trxDate;
    
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
    
    private BigDecimal subTotal;
    private BigDecimal diskon;
    private BigDecimal ongkir;
    private BigDecimal paymentTotal;

    public Sales() {
    }

    public Sales(Long id, String code, Date trxDate, Customer customer, BigDecimal subTotal, BigDecimal diskon, BigDecimal ongkir, BigDecimal paymentTotal) {
        this.id = id;
        this.code = code;
        this.trxDate = trxDate;
        this.customer = customer;
        this.subTotal = subTotal;
        this.diskon = diskon;
        this.ongkir = ongkir;
        this.paymentTotal = paymentTotal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(Date trxDate) {
        this.trxDate = trxDate;
    }

    @JsonView(DataTablesOutput.View.class)
    public String getTrxDateFormatted() {
         SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(trxDate);
    }
    
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getDiskon() {
        return diskon;
    }

    public void setDiskon(BigDecimal diskon) {
        this.diskon = diskon;
    }

    public BigDecimal getOngkir() {
        return ongkir;
    }

    public void setOngkir(BigDecimal ongkir) {
        this.ongkir = ongkir;
    }

    public BigDecimal getPaymentTotal() {
        return paymentTotal;
    }

    public void setPaymentTotal(BigDecimal paymentTotal) {
        this.paymentTotal = paymentTotal;
    }
    
}
