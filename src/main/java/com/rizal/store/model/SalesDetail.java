/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizal.store.model;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author GodSpeed
 */
@Entity
@Table(name = "sales_details")
public class SalesDetail {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "sales_id")
    private Sales sales;
    
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    
    private BigDecimal price;
    private Long qty;
    private BigDecimal diskonPct;
    private BigDecimal diskonNilai;
    private BigDecimal priceDiskon;
    private BigDecimal total;

    public SalesDetail() {
    }

    public SalesDetail(Long id, Sales sales, Product product, BigDecimal price, Long qty, BigDecimal diskonPct, BigDecimal diskonNilai, BigDecimal priceDiskon, BigDecimal total) {
        this.id = id;
        this.sales = sales;
        this.product = product;
        this.price = price;
        this.qty = qty;
        this.diskonPct = diskonPct;
        this.diskonNilai = diskonNilai;
        this.priceDiskon = priceDiskon;
        this.total = total;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Sales getSales() {
        return sales;
    }

    public void setSales(Sales sales) {
        this.sales = sales;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getQty() {
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    public BigDecimal getDiskonPct() {
        return diskonPct;
    }

    public void setDiskonPct(BigDecimal diskonPct) {
        this.diskonPct = diskonPct;
    }

    public BigDecimal getDiskonNilai() {
        return diskonNilai;
    }

    public void setDiskonNilai(BigDecimal diskonNilai) {
        this.diskonNilai = diskonNilai;
    }

    public BigDecimal getPriceDiskon() {
        return priceDiskon;
    }

    public void setPriceDiskon(BigDecimal priceDiskon) {
        this.priceDiskon = priceDiskon;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
    
    
}
