/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizal.store.controller;

import com.rizal.store.model.Sales;
import com.rizal.store.model.SalesDetail;
import com.rizal.store.model.form.SalesForm;
import com.rizal.store.service.CustomerService;
import com.rizal.store.service.ProductService;
import com.rizal.store.service.SalesDetailService;
import com.rizal.store.service.SalesService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author GodSpeed
 */
@Controller
@RequestMapping("/sales")
public class SalesController {

    private final SalesService salesService;
    private final SalesDetailService detailService;
    private final CustomerService customerService;
    private final ProductService productService;

    @Autowired
    public SalesController(SalesService salesService, SalesDetailService detailService, CustomerService customerService, ProductService productService) {
        this.salesService = salesService;
        this.detailService = detailService;
        this.customerService = customerService;
        this.productService = productService;
    }

    @GetMapping("/index")
    public String viewHomePage(Model model) {
        model.addAttribute("rows", detailService.getAll());
        return "index";
    }

    @GetMapping("/new")
    public String newForm(Model model) {
        Sales sales = new Sales(); // Buat instance baru dari Sales
        List<SalesDetail> details = new ArrayList<>(); // Buat list baru untuk SalesDetail
        SalesForm form = new SalesForm(sales, details);
        model.addAttribute("row", form);
        model.addAttribute("customers", customerService.getAllCustomers());
        model.addAttribute("products", productService.getAllProducts());
        return "sales-form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("row") SalesForm form, Model model) {
        salesService.save(form);
        return "redirect:/sales/index";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable Long id, Model model) {
        Optional<Sales> sales = salesService.getSalesById(id);
        model.addAttribute("row", sales);
        return "sales-form";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        salesService.delete(id);
        return "redirect:/sales/index";
    }
}
