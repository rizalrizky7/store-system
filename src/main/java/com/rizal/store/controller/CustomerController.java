/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizal.store.controller;

import com.rizal.store.model.Customer;
import com.rizal.store.service.CustomerService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author GodSpeed
 */
@Controller
@RequestMapping("/customer")
public class CustomerController{
    
    private final CustomerService customerService;
    
    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }
    
    @GetMapping("/index")
    public String viewHomePage(Model model) {
        model.addAttribute("customers", customerService.getAllCustomers());
        return "customer";
    }

    @GetMapping("/new")
    public String newForm(Model model) {
        Customer customer = new Customer();
        model.addAttribute("row", customer);
        return "customer-form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("product") Customer customer) {
        customerService.save(customer);
        return "redirect:/customer/index";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable Long id, Model model) {
        Optional<Customer> customer = customerService.getCustomerById(id);
        model.addAttribute("row", customer);
        return "customer-form";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        customerService.delete(id);
        return "redirect:/customer/index";
    }

}
