/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizal.store.repositories;

import com.rizal.store.model.SalesDetail;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author GodSpeed
 */
public interface SalesDetailRepository extends JpaRepository<SalesDetail, Long>{
    
    @Query("SELECT sd FROM SalesDetail sd WHERE sd.sales.id = ?1")
    List<SalesDetail> selectBySalesId(Long id);
}
