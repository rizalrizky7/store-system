/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rizal.store.repositories;

import com.rizal.store.model.Sales;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author GodSpeed
 */
public interface SalesRepository extends JpaRepository<Sales, Long> {
    
    @Query("SELECT MAX(SUBSTRING(j.code, LENGTH(j.code) - 3)) FROM Sales j WHERE j.trxDate = ?1")
    String getLastId(Date transactionDate);
    
}
