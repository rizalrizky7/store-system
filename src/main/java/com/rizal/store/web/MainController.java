package com.rizal.store.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

//    private UserRepository userRepository;
//
//    @Autowired
//    public MainController(UserRepository userRepository) {
//        this.userRepository = userRepository;
//    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

//    @PostMapping("/login")
//    public String index(String username, String password) {
//
//        User user = userRepository.findByUsername(username);
//        if (user == null) {
//            throw new UsernameNotFoundException("Invalid username or password.");
//        }
//        
//        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//        if (!encoder.matches(password, user.getPassword())) {
//            throw new UsernameNotFoundException("Invalid username or password.");
//        }
//
//        return "index";
//    }

}
