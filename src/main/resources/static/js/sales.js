var Sales = (function () {

    // Fungsi untuk menambahkan produk ke tabel detail
    function addProductToDetails(productId, productName, retailPrice, productCode) {
        var detailsTableBody = document.getElementById("detailsTable").getElementsByTagName("tbody")[0];

        var row = detailsTableBody.insertRow(-1);

        var deleteCell = row.insertCell(0);
        var noCell = row.insertCell(1);
        var productCodeCell = row.insertCell(2);
        var productNameCell = row.insertCell(3);
        var qtyCell = row.insertCell(4);
        var retailPriceCell = row.insertCell(5);
        var discountPercentageCell = row.insertCell(6);
        var discountAmountCell = row.insertCell(7);
        var discountedPriceCell = row.insertCell(8);
        var totalCell = row.insertCell(9);
        var productIdCell = row.insertCell(10);
        var productPriceCell = row.insertCell(11);

        deleteCell.innerHTML = '<a class="btn btn-danger btn-sm mb-3 ml-2 deleteRowBtn">Delete</a>';
        noCell.textContent = detailsTableBody.rows.length;
        productCodeCell.innerHTML = '<input class="form-control" type="text" name="details[' + (detailsTableBody.rows.length - 1) + '].product.code" value="' + productCode + '" readonly>';
        productNameCell.innerHTML = '<input class="form-control" type="text" name="details[' + (detailsTableBody.rows.length - 1) + '].product.name" value="' + productName + '" readonly>';
        qtyCell.innerHTML = '<input class="form-control" type="number" name="details[' + (detailsTableBody.rows.length - 1) + '].qty" value="" required="true">';
        retailPriceCell.innerHTML = '<input class="form-control" type="number" name="details[' + (detailsTableBody.rows.length - 1) + '].price" value="' + parseInt(retailPrice) + '" readonly>';
        discountPercentageCell.innerHTML = '<input class="form-control" type="number" name="details[' + (detailsTableBody.rows.length - 1) + '].diskonPct" value="">';
        discountAmountCell.innerHTML = '<input class="form-control" type="number" name="details[' + (detailsTableBody.rows.length - 1) + '].diskonNilai" value="" readonly>';
        discountedPriceCell.innerHTML = '<input class="form-control" type="number" name="details[' + (detailsTableBody.rows.length - 1) + '].priceDiskon" value="" readonly>';
        totalCell.innerHTML = '<input class="form-control" type="number" name="details[' + (detailsTableBody.rows.length - 1) + '].total" value="" readonly>';
        productIdCell.innerHTML = '<input class="form-control" type="hidden" name="details[' + (detailsTableBody.rows.length - 1) + '].product.id" value="' + productId + '" readonly>';
        productPriceCell.innerHTML = '<input class="form-control" type="hidden" name="details[' + (detailsTableBody.rows.length - 1) + '].product.price" value="' + retailPrice + '" readonly>';
        productIdCell.style.display = "none";
        productPriceCell.style.display = "none";
    };


    function formatCurrency(input) {
        // Remove non-numeric characters
        var value = input.value.replace(/[^0-9]/g, '');

        // Format as currency
        var formatter = new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',
            minimumFractionDigits: 0,
        });
        input.value = formatter.format(value);
    };
    
    var eventHandler = function () {
        $(document).on('click', '#selectCustomerLink', function () {
            $('#customerModal').modal('show');
        }).on('click', '.selectCustomerBtn', function () {
            // Ambil nama customer yang dipilih
            var selectedCustomerId = $(this).closest('tr').find('td:first').text();
            var selectedCustomerName = $(this).closest('tr').find('td:nth-child(2)').text();
            var selectedCustomerCode = $(this).closest('tr').find('td:nth-child(3)').text();
            var selectedCustomerPhone = $(this).closest('tr').find('td:nth-child(4)').text();
            // Lakukan tindakan sesuai dengan customer yang dipilih
            $('#customerId').val(selectedCustomerId);
            $('#customerCode').val(selectedCustomerCode);
            $('#customerName').val(selectedCustomerName);
            $('#customerPhone').val(selectedCustomerPhone);
            console.log('Selected customer: ' + selectedCustomerId);
            // Sembunyikan modal setelah customer dipilih
            $('#customerModal').modal('hide');
        }).on('click', '.closeCustomerSelect', function () {
            $('#customerModal').modal('hide');
        }).on('click', '#selectProduct', function () {
            $('#productModal').modal('show');
        }).on('click', '.selectProductBtn', function () {
            // Ambil nama customer yang dipilih
            var selectedProductId = $(this).closest('tr').find('td:first').text();
            var selectedProductName = $(this).closest('tr').find('td:nth-child(2)').text();
            var selectedProductrPrice = $(this).closest('tr').find('td:nth-child(3)').text();
            var selectedProductrCode = $(this).closest('tr').find('td:nth-child(4)').text();
            // Lakukan tindakan sesuai dengan customer yang dipilih
            addProductToDetails(selectedProductId, selectedProductName, selectedProductrPrice, selectedProductrCode);
            // Sembunyikan modal setelah customer dipilih
            $('#productModal').modal('hide');
        }).on('click', '.closeProductBtn', function () {
            $('#productModal').modal('hide');
        }).on('click', '.deleteRowBtn', function () {
            // Handle delete button click
            var row = $(this).closest('tr');
            row.remove();
        }).on('input', '#diskon', function () {
            var diskon = parseInt($(this).val());
            var ongkir = $('#ongkir').val();
            var subTotal = $('#subTotal').val();
            var paymentTotal = subTotal - diskon + ongkir;
            $('#totalPayment').val(paymentTotal);
        }).on('input', '#ongkir', function () {
            var diskon = $('#diskon').val();
            var ongkir = parseInt($(this).val());
            var subTotal = $('#subTotal').val();
            var paymentTotal = subTotal - diskon + ongkir;
            $('#totalPayment').val(paymentTotal);
        }).on('input', 'input[name^="details["][name$="].qty"]', function () {
            // Handle quantity input change
            var row = $(this).closest('tr');
            var qty = parseInt($(this).val());
            var discountedPrice = row.find('input[name$="].priceDiskon"]').val();
            var retailPrice = row.find('input[name$="].price"]').val();
            var total;
            if (discountedPrice === "" || discountedPrice === "0") {
                total = qty * retailPrice;
            } else {
                total = qty * discountedPrice;
            }
            row.find('input[name$="].total"]').val(total);
            var subTotal = 0;
            $('input[name$="].total"]').each(function () {
                var rowTotal = parseFloat($(this).val());
                if (!isNaN(rowTotal)) {
                    subTotal += rowTotal;
                }
            });
            $('#subTotal').val(subTotal);
            $('#totalPayment').val(subTotal);
        }).on('input', 'input[name^="details["][name$="].diskonPct"]', function () {
            // Handle quantity input change
            var row = $(this).closest('tr');
            var percentage = parseInt($(this).val());
            var retailPrice = row.find('input[name$="].price"]').val();
            var qty = row.find('input[name$="].qty"]').val();
            var totalDiskon = retailPrice / 100 * percentage;
            var discountedPrice = retailPrice - totalDiskon;
            var total = qty * discountedPrice;
            row.find('input[name$="].diskonNilai"]').val(totalDiskon);
            row.find('input[name$="].priceDiskon"]').val(discountedPrice);
            row.find('input[name$="].total"]').val(total);
            var subTotal = 0;
            $('input[name$="].total"]').each(function () {
                var rowTotal = parseFloat($(this).val());
                if (!isNaN(rowTotal)) {
                    subTotal += rowTotal;
                }
            });
            $('#subTotal').val(subTotal);
            $('#totalPayment').val(subTotal);
        });
    };
    return {
        init: function () {
            eventHandler();
        }
    };
})();
$(document).ready(function () {
    Sales.init();
});
